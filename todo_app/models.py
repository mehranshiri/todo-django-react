from django.db import models
from accounts.models import User


class ToDos(models.Model):
    title = models.CharField(max_length=128, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False)
