from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from todo_app.models import ToDos
from todo_app.serializers import ToDoSerializer


class ToDoListView(APIView):
    """
    List all todos, or create a new todo.
    """

    permission_classes = [IsAuthenticated]
    serializer_class = ToDoSerializer

    def get(self, request, format=None):
        completed = int(request.query_params.get('completed') == 'true')
        todos = ToDos.objects.filter(user=request.user.id, completed=completed)
        serializer = ToDoSerializer(todos, many=True)
        return Response({'items': serializer.data})

    def post(self, request, format=None):
        # Copy parsed content from HTTP request
        data = request.data.copy()

        # Add id of currently logged user
        data['user'] = request.user.id
        serializer = ToDoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ToDoDetailView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ToDoSerializer

    """
    Retrieve, update or delete a todo instance.
    """

    def get_object(self, pk):
        try:
            return ToDos.objects.get(pk=pk)
        except ToDos.DoesNotExist:
            raise status.HTTP_404_NOT_FOUND

    def get(self, request, id, format=None):
        try:
            todo = self.get_object(id)
            serializer = ToDoSerializer(todo)
            return Response(serializer.data)
        except ToDos.DoesNotExist:
            return Response({'error': 'Not found!'}, status=status.HTTP_404_NOT_FOUND)

    def put(self, request, id, format=None):
        todo = self.get_object(id)
        # Copy parsed content from HTTP request
        data = request.data.copy()

        # Add id of currently logged user
        data['user'] = request.user.id
        serializer = ToDoSerializer(todo, data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, format=None):
        todo = self.get_object(id)
        todo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
