from django.urls import path
from todo_app.views import ToDoDetailView, ToDoListView

urlpatterns = [
    path('todos', ToDoListView.as_view()),
    path('todo/<int:id>', ToDoDetailView.as_view()),

]
