from rest_framework import serializers
from todo_app.models import ToDos
from accounts.serializers import UserSerializer


class ToDoSerializer(serializers.ModelSerializer):

    title = serializers.CharField(max_length=128)

    class Meta:
        model = ToDos
        fields = ['id', 'title', 'completed', 'user']
        # fields = '__all__'

    def create(self, validated_data):
        return ToDos.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.completed = validated_data.get('completed', instance.completed)
        instance.save()
        return instance
