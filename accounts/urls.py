from django.urls import path

from accounts.views import RegisterView, LoginView, IsAuthorizedView, LogoutView

urlpatterns = [
    path('register', RegisterView.as_view(), name="register"),
    path('login', LoginView.as_view(), name='login'),
    path('is-authorized', IsAuthorizedView.as_view()),
    path('logout', LogoutView.as_view())
]
