from django.contrib import auth
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from accounts.models import User
from accounts.serializers import UserSerializer, LoginSerializer


# Register user
class RegisterView(GenericAPIView):
    serializer_class = UserSerializer

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            token, created = Token.objects.get_or_create(user=user)
            data = {'user': serializer.data, 'token': token.key}
            return Response(data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        data = request.data
        username = data.get('username', '')
        password = data.get('password', '')
        user = auth.authenticate(username=username, password=password)

        if user:
            serializer = UserSerializer(user)
            token, created = Token.objects.get_or_create(user=user)
            data = {'user': serializer.data, 'token': token.key}
            return Response(data, status=status.HTTP_200_OK)

        return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)


class IsAuthorizedView(GenericAPIView):
    serializer_class = UserSerializer

    def get(self, request):
        if request.user.is_authenticated:
            instance = User.objects.get(id=request.user.id)
            serializer = UserSerializer(instance, context={'request': request})
            return Response({'logged_in': True, 'user': serializer.data})
        else:
            return Response({'logged_in': False, 'error': 'Unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(GenericAPIView):
    serializer_class = UserSerializer

    def post(self, request):
        if request.user.is_authenticated:
            request.user.auth_token.delete()
            return Response(status=status.HTTP_200_OK)

        return Response({'logged_In': False, 'error': 'Unauthorized'}, status=status.HTTP_401_UNAUTHORIZED)