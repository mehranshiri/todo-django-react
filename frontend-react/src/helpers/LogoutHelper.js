import logout from "../services/api/repository/v1/logout";
import {removeStorage} from "./LocalStorageHelpers";

export default function Logout() {

    logout.post()
        .then(() => {
            removeStorage('todo_access_token')
        })
}