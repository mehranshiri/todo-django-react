import React from 'react';
import {useFormik} from "formik";
import * as Yup from 'yup';
import login from "../../services/api/repository/v1/login";
import {setStorage} from "../../helpers/LocalStorageHelpers";
import {Alert, Button, Col, Container, Form, Row} from "react-bootstrap";
import {useHistory} from 'react-router-dom';

const initialValues = {
    email: "test@gmail.com",
    password: "123456",
};

function Login() {

    const history = useHistory();

    const LoginSchema = Yup.object().shape({
        email: Yup.string()
            .email("Wrong email format")
            .min(3, "Minimum 3 symbols")
            .max(50, "Maximum 50 symbols")
            .required(),
        password: Yup.string()
            .min(3, "Minimum 3 symbols")
            .max(50, "Maximum 50 symbols")
            .required(),
    });

    const getInputClasses = (fieldname) => {
        if (formik.touched[fieldname] && formik.errors[fieldname]) {
            return "has-error";
        }

        if (formik.touched[fieldname] && !formik.errors[fieldname]) {
            return "has-success";
        }

        return "";
    };

    const formik = useFormik({
        initialValues,
        validationSchema: LoginSchema,
        onSubmit: (values, {setStatus, setSubmitting}) => {

            login.post(
                {
                    'username': values.email,
                    'password': values.password
                }
            )
                .then((response) => {
                    setStatus({
                        success: true
                    });
                    setStorage('todo_access_token', response.data.token);
                    history.push('/')

                })
                .catch((error) => {
                    setSubmitting(false);
                    setStatus({
                        success: false
                    });
                });
        },
    });

    const goToRegister = () => {
        history.push('/register')
    };

    return (
        <Container>
            <Row style={{height: '100vh'}}>
                <Col xs={{span: 6, offset: 3}} className="text-center align-self-center">
                    <Form
                        onSubmit={formik.handleSubmit}
                    >
                        {formik.status ? (
                            formik.status.success
                                ?
                                <Alert variant="success">
                                    Successfull!
                                </Alert>
                                :
                                <Alert variant="danger">
                                    Incorrect email or password!
                                </Alert>
                        ) : (
                            null
                        )}

                        <div className="form-group ">
                            <input
                                placeholder="Email"
                                type="email"
                                className={`form-control ${getInputClasses(
                                    "email"
                                )}`}
                                name="email"
                                {...formik.getFieldProps("email")}
                            />
                            {formik.touched.email && formik.errors.email ? (
                                <Alert variant="danger">
                                    {formik.errors.email}
                                </Alert>
                            ) : null}
                        </div>
                        <div className="form-group">
                            <input
                                placeholder="Password"
                                type="password"
                                className={`form-control ${getInputClasses(
                                    "password"
                                )}`}
                                name="password"
                                {...formik.getFieldProps("password")}
                            />
                            {formik.touched.password && formik.errors.password ? (
                                <Alert variant="danger">
                                    {formik.errors.password}
                                </Alert>
                            ) : null}
                        </div>
                        <div className="form-group d-flex flex-wrap justify-content-between align-items-center">
                            <Button variant="primary" type="submit">
                                Sign In
                            </Button>
                            <Button variant="secondary" onClick={goToRegister}>
                                Sign Up
                            </Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

export default Login;