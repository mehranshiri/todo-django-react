import React from 'react';
import {Col, Container, Row} from "react-bootstrap";
import RegisterForm from "./RegisterForm";

function Register() {

    return (
        <Container>
            <Row style={{height: '100vh'}}>
                <Col xs={{span: 6, offset: 3}} className="text-center align-self-center">
                    <RegisterForm/>
                </Col>
            </Row>
        </Container>
    );
}

export default Register;