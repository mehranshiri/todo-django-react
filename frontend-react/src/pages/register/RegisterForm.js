import React from 'react';
import {useFormik} from "formik";
import * as Yup from 'yup';
import register from "../../services/api/repository/v1/register";
import {setStorage} from "../../helpers/LocalStorageHelpers";
import {Alert, Button, Form} from "react-bootstrap";
import { useHistory } from 'react-router-dom';

const initialValues = {
    name: "",
    email: "",
    password: "",
};

export default function RegisterForm() {

    const history = useHistory();

    const LoginSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, "Minimum 3 symbols")
            .max(50, "Maximum 50 symbols")
            .required(),
        email: Yup.string()
            .email("Wrong email format")
            .min(3, "Minimum 3 symbols")
            .max(50, "Maximum 50 symbols")
            .required(),
        password: Yup.string()
            .min(3, "Minimum 3 symbols")
            .max(50, "Maximum 50 symbols")
            .required(),
    });

    const getInputClasses = (fieldname) => {
        if (formik.touched[fieldname] && formik.errors[fieldname]) {
            return "has-error";
        }

        if (formik.touched[fieldname] && !formik.errors[fieldname]) {
            return "has-success";
        }

        return "";
    };

    const formik = useFormik({
        initialValues,
        validationSchema: LoginSchema,
        onSubmit: (values, {setStatus, setSubmitting}) => {
            setTimeout(() => {

                register.post(
                    {
                        'name': values.name,
                        'email': values.email,
                        'password': values.password
                    }
                )
                    .then((response) => {
                        setStatus({
                            success: true
                        });
                        setStorage('todo_access_token', response.data.token);
                        history.push('/')

                    })
                    .catch(() => {
                        setSubmitting(false);
                        setStatus({
                            success: false
                        });
                    });
            }, 1000);
        },
    });

    const goToLogin = () => {
        history.push('/login')
    };

    return (
        <Form
            onSubmit={formik.handleSubmit}
        >
            {formik.status && (
                formik.status.success
                    ?
                    <Alert variant="success">
                        Registerd successfully!
                    </Alert>
                    :
                    <Alert variant="danger">
                        Something went wrong, Please try again!
                    </Alert>
            )}

            <div className="form-group">
                <input
                    placeholder="Your Name"
                    type="text"
                    className={`form-control ${getInputClasses(
                        "name"
                    )}`}
                    name="name"
                    {...formik.getFieldProps("name")}
                />
                {formik.touched.name && formik.errors.name ? (
                    <Alert variant="danger">
                        {formik.errors.name}
                    </Alert>
                ) : null}
            </div>

            <div className="form-group ">
                <input
                    placeholder="Email"
                    type="email"
                    className={`form-control ${getInputClasses(
                        "email"
                    )}`}
                    name="email"
                    {...formik.getFieldProps("email")}
                />
                {formik.touched.email && formik.errors.email ? (
                    <Alert variant="danger">
                        {formik.errors.email}
                    </Alert>
                ) : null}
            </div>
            <div className="form-group">
                <input
                    placeholder="Password"
                    type="password"
                    className={`form-control ${getInputClasses(
                        "password"
                    )}`}
                    name="password"
                    {...formik.getFieldProps("password")}
                />
                {formik.touched.password && formik.errors.password ? (
                    <Alert variant="danger">
                        {formik.errors.password}
                    </Alert>
                ) : null}
            </div>
            <div className="form-group d-flex flex-wrap justify-content-between align-items-center">
                <Button variant="primary" type="submit">
                    Sign Up
                </Button>

                <Button variant="secondary" onClick={goToLogin}>
                    Sign In
                </Button>
            </div>
        </Form>
    )
}