import React, {useEffect, useState} from "react";
import TodoItemReq from "../../services/api/repository/v1/todo-item";

export default function TodoItems(props) {

    function handleDelete(id) {
        TodoItemReq.del(id)
        props.onSave();
    }

    function handleChange(id) {
        TodoItemReq.get(id)
            .then((response) => {
                console.log(response.data);
            })
        props.onSave();
    }

    return (
        <>
            {props.items && props.items.map((item, key) =>
                <li
                    key={key}
                    className="list-group-item d-flex justify-content-between align-items-center"
                >
                    <span className="todo-title mr-2">
                      {item.title}
                    </span>
                    <span>
                          <button className="btn btn-secondary mr-2" onClick={() => handleChange(item.id)}> Edit </button>
                          <button className="btn btn-danger" onClick={() => handleDelete(item.id)}>Delete </button>
                    </span>
                </li>
            )}
        </>
    )
}