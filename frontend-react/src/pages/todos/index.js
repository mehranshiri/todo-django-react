import React, {useEffect, useState} from "react";
import CustomModal from "../../components/Modal";
import TodoListReq from "../../services/api/repository/v1/todo-list";
import TodoItemReq from "../../services/api/repository/v1/todo-item";
import Logout from "../../helpers/LogoutHelper";
import { useHistory } from 'react-router-dom';

export default function Todos() {

    const [viewCompleted, setViewCompleted] = useState(true);
    const [openModal, setOpenModal] = useState(false);
    const [items, setItems] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [activeItem, setActiveItem] = useState({
        'title': '',
        'completed': false
    });
    const history = useHistory();

    useEffect(() => {
        TodoListReq.get(viewCompleted)
            .then((response) => {
                setItems(response.data.items);
                setIsLoading(false);
            })
    }, [isLoading, viewCompleted]);

    const handleAdd = () => {
        setOpenModal(true);
        setActiveItem({
            'title': '',
            'completed': false
        })
    };

    const handleChange = id => {
        TodoItemReq.get(id)
            .then((response) => {
                setActiveItem({
                    'id': response.data.id,
                    'title': response.data.title,
                    'completed': response.data.completed
                })
            });
        setOpenModal(true)
    };

    const handleDelete = id => {
        TodoItemReq.del(id);
        alert('Deleted successfully!');
        setIsLoading(true);
    };

    const handleLogout = () => {
        Logout();
        history.push('/login')
    }

    const closeModal = () => {
        setOpenModal(false)
    };

    const refreshList = () => {
        setIsLoading(true);
        setOpenModal(false)
    };

    return (
        <main className="container">
            <h1 className="text-uppercase text-center my-4">Todo app</h1>
            <div className="row ">
                <div className="col-md-6 col-sm-10 mx-auto p-0">
                    <div className="card p-3">
                        <ul className="list-group list-group-flush">
                            <div className="form-group d-flex flex-wrap justify-content-between align-items-center">
                                <button className="btn btn-primary" onClick={handleAdd}>Add task</button>
                                <button className="btn btn-warning" onClick={handleLogout}>Logout</button>
                            </div>
                            <div className="my-5 tab-list">
                                <button
                                    onClick={() => setViewCompleted(true)}
                                    className={`btn btn-outline-dark mr-3 ${viewCompleted ? "active" : ""}`}
                                >
                                  completed
                                </button>
                                <button
                                    onClick={() => setViewCompleted(false)}
                                    className={`btn btn-outline-dark ${!viewCompleted ? "active" : ""}`}
                                >
                                  Going on
                                </button>
                            </div>

                            {items && items.map((item, key) =>
                                <li
                                    key={key}
                                    className="list-group-item d-flex justify-content-between align-items-center"
                                >
                                    <span className="todo-title mr-2">
                                      {item.title}
                                    </span>
                                    <span>
                                          <button className="btn btn-secondary mr-2"
                                                  onClick={() => handleChange(item.id)}> Edit </button>
                                          <button className="btn btn-danger"
                                                  onClick={() => handleDelete(item.id)}>Delete </button>
                                    </span>
                                </li>
                            )}

                        </ul>
                    </div>
                </div>
            </div>
            {openModal ? (
                <CustomModal
                    activeItem={activeItem}
                    toggle={closeModal}
                    onSave={refreshList}
                />
            ) : null}
        </main>
    )
}