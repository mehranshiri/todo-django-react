import React from "react";
import {BrowserRouter as Router, Switch} from 'react-router-dom';
import ProtectedRoute from "./components/ProtectedRoute";
import Login from "./pages/login";
import Register from "./pages/register";
import Todos from "./pages/todos";

function App() {
    return (
        <Router>
            <Switch>
                <ProtectedRoute
                    exact={true}
                    path='/register'
                    component={Register}
                />
                <ProtectedRoute
                    exact={true}
                    path='/login'
                    component={Login}
                />
                <ProtectedRoute
                    exact={true}
                    path="/"
                    component={Todos}
                    protected={true}
                />
            </Switch>
        </Router>

    );
}

export default App;
