import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import reportWebVitals from './reportWebVitals';
import reducer from "./redux/reducer";
import {createStore} from "redux";
import {Provider} from "react-redux";
const rdx = createStore(reducer);

ReactDOM.render(
    <Provider store={rdx}>
            <App />
    </Provider>,
  document.getElementById('root')
);

reportWebVitals();
