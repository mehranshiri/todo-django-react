import React, {useEffect, useState} from "react";
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import TodoItemReq from "../services/api/repository/v1/todo-item";

export default function CustomModal(props) {

    const [activeItem, setActiveItem] = useState({
        'title': '',
        'completed': false
    });

    useEffect(() => {
        setActiveItem(props.activeItem);
        console.log(activeItem.title)
    }, [props.activeItem]);

    function handleChange(e) {
        let {name, value} = e.target;
        if (e.target.type === "checkbox") {
            value = e.target.checked;
        }
        const obj = {...activeItem, [name]: value};
        setActiveItem(obj);
    };

    const handleSubmit = () => {
        if(!activeItem.id) {
            TodoItemReq.post(activeItem)
                .then(() => {
                    onSave()
                })
        } else {
            TodoItemReq.put(activeItem)
                .then(() => {
                    onSave()
                })
        }
    };

    const {toggle, onSave} = props;

    return (
        <Modal isOpen={true} toggle={toggle}>
            <ModalHeader toggle={toggle}> Todo Item </ModalHeader>
            <ModalBody>
                <Form>
                    <FormGroup>
                        <Label for="title">Title</Label>
                        <Input
                            type="text"
                            name="title"
                            value={activeItem.title}
                            onChange={(e) => handleChange(e)}
                            placeholder="Enter Todo Title"
                        />
                    </FormGroup>
                    <FormGroup check>
                        <Label for="completed">
                            <Input
                                type="checkbox"
                                name="completed"
                                checked={activeItem.completed}
                                onChange={(e) => handleChange(e)}
                            />
                            Completed
                        </Label>
                    </FormGroup>
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={() => handleSubmit(activeItem)}>
                    Save
                </Button>
            </ModalFooter>
        </Modal>
    );
}