import React, {useEffect, useState} from "react";
import {Redirect, Route} from 'react-router-dom';
import isAuthenticated from '../services/api/repository/v1/is-authenticated';
import { useDispatch } from "react-redux";

export default function ProtectedRoute(props) {

    const [isLoggedIn, setIsLoggedIn] = useState();
    const [user, setUser] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const dispatch = useDispatch();

    useEffect(() => {
        if (props) {

            isAuthenticated
                .get()
                .then((response) => {
                    const data = response.data;
                    setIsLoggedIn(data.logged_in);
                    setUser(data.user);
                    setIsLoading(false);
                    dispatch({
                        type: 'handleUsername',
                        value: data.user.name
                    });
                }).catch(() => {
                setIsLoading(false)
            });
        }
    }, [props]);

    return (
        <>
            {
                isLoading ?
                    null
                    :
                    <Route
                        path={props.path}
                        render={(k) =>
                            props.protected
                                ?
                                isLoggedIn
                                    ? <props.component exact={props.exact} {...k}
                                                       loggedInStatus={isLoggedIn} User={user}/>
                                    :
                                    <Redirect to={{
                                        pathname: '/login',
                                        redirect: true
                                    }}/>
                                : <props.component exact={props.exact}  {...k} kk={k}
                                                   loggedInStatus={isLoggedIn} User={user}/>
                        }/>
            }
        </>
    )

}
