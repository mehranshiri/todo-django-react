function reducer(state = {
    loading: true,
    user: null,
    cartItems: 0
}, action) {

    switch (action.type) {
        case "handleLoading":
            return {
                ...state,
                loading: action.value
            };
        case "handleUser":
            return {
                ...state,
                user: action.value
            };
        case "handleCartItems":
            return {
                ...state,
                cartItems: parseInt(action.value, 10)
            };
        default:
            return state;
    }
}

export default reducer;