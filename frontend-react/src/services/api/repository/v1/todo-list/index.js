import request from '../../../../../services/api/request';
import getContext from '../../../../../context';
import {getStorage} from "../../../../../helpers/LocalStorageHelpers";

function get(completed) {

    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Token ' + getStorage('todo_access_token')
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/todos?completed=' + completed,
        method: 'get',
        headers: headers
    });

}
const TodoListReq = {
    get
};

export default TodoListReq;