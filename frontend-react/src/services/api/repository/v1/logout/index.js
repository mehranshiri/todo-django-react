import request from '../../../../../services/api/request';
import getContext from '../../../../../context';
import {getStorage} from "../../../../../helpers/LocalStorageHelpers";

function post() {

    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Token ' + getStorage('todo_access_token')
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/logout',
        method: 'post',
        headers: headers
    });
}

const logout = {
    post
};

export default logout;