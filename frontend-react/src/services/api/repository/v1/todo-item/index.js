import request from '../../../../../services/api/request';
import getContext from '../../../../../context';
import {getStorage} from "../../../../../helpers/LocalStorageHelpers";

function get(id) {
    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Token ' + getStorage('todo_access_token')
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/todo/' + id,
        method: 'get',
        headers: headers
    });
}

function post(data) {

    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Token ' + getStorage('todo_access_token')
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/todos',
        method: 'post',
        data: data,
        headers: headers
    });
}

function put(data) {

    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Token ' + getStorage('todo_access_token')
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/todo/' + data.id,
        method: 'put',
        data: data,
        headers: headers
    });
}

function del(id) {

    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Token ' + getStorage('todo_access_token')
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/todo/' + id,
        method: 'delete',
        headers: headers
    });
}

const TodoItemReq = {
    get,
    post,
    put,
    del
};

export default TodoItemReq;