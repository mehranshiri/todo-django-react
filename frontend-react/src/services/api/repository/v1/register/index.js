import request from '../../../../../services/api/request';
import getContext from '../../../../../context';

function post(data) {

    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/register',
        method: 'post',
        data: data,
        headers: headers
    });
}

const register = {
    post
};

export default register;