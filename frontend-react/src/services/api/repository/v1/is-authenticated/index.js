import request from '../../../../../services/api/request';
import getContext from '../../../../../context';
import {getStorage} from "../../../../../helpers/LocalStorageHelpers";

function get() {

    let headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': 'Token ' + getStorage('todo_access_token')
    };

    return request({
        baseURL: getContext.baseUrl,
        url: '/is-authorized',
        method: 'get',
        headers: headers
    });

}
const isAuthenticated = {
    get
};

export default isAuthenticated;