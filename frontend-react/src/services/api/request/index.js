import axios from "axios";

function Request(options, showLoading) {


    /**
     * Request Wrapper with default success/error actions
     * Send option to client that create in axios
     */
    const client = axios.create();


    function onSuccess(response) {

        return response;
    }

    function onError(error) {

        return Promise.reject(error.response || error.message);
    }

    return client(options)
        .then(onSuccess)
        .catch(onError)
}
export default Request
